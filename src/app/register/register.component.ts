import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../_services/user.service';
import { Http,Headers } from '@angular/http';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  providers:[UserService]
})
export class RegisterComponent {
  model1: any = {};
  universities: any;
  designations: any;
  alma_meters: any;
  courses: any;
  model: any = {
    Student: '1',
    gender: '1'
};

type: any = 'Academic';

constructor(private _userService:UserService,private http:Http){

}
regiAs(newValue: number) {
  this.model = {
      Student: '1',
      gender: '1'
  };

}


ngOnInit() {

  // get universities data

  this._userService.getUniversities().subscribe(university =>{
   this.universities = university;
  });

  // get designations data

  this._userService.getdesignations().subscribe(designation =>{
    this.designations = designation;
  });

  // get alma meters data

  this._userService.getalmameter().subscribe(alma =>{
    this.alma_meters = alma;
  });

  // get course data
  this._userService.getcourse().subscribe(course =>{
    this.courses = course;
  });

}
}

