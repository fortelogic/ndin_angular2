import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { LoginComponent } from '../app/login/login.component';
import { RegisterComponent } from '../app/register/register.component';
import { HeaderComponent } from '../app/common/header/header.component';
import { SolutionspaceComponent } from '../app/solution/solutionspace.component';
import { ProfileComponent } from '../app/profile/profile.component';
import { LeftbarComponent } from '../app/common/leftbar/leftbar.component';
import { routing } from './app.routing';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    SolutionspaceComponent,
    LeftbarComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
