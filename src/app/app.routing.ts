import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SolutionspaceComponent } from './solution/solutionspace.component';
import { ProfileComponent } from './profile/profile.component';
const appRoutes: Routes = [
    { path: '', component: RegisterComponent },
    { path: 'login', component: LoginComponent },
    // { path: 'announcement', component: AnnouncementComponent, canActivate: [AuthGuard] },
    { path: 'register', component: RegisterComponent },
    { path: 'profile', component: ProfileComponent },
    { path: 'solutionspace', component: SolutionspaceComponent },
    // { path: 'solutionspace', component: SolutionspaceComponent },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
