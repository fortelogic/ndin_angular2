import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class UserService{

  constructor(private http: Http){

  }
// get universities data
  getUniversities(){

     return this.http.get('http://ndin.fortelogic.in/universities.json').map(res => res.json());
  }

  // get designation api
  getdesignations(){
    return this.http.get('http://ndin.fortelogic.in/designations.json').map(res => res.json());
  }

  // get alma meters api
  getalmameter(){
    return this.http.get('http://ndin.fortelogic.in/alma_maters.json').map(res => res.json());
  }

  // get course api
  getcourse(){
    return this.http.get('http://ndin.fortelogic.in/courses.json').map(res => res.json());
  }

}
